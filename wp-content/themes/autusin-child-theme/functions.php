<?php
add_action( 'wp_enqueue_scripts', 'ya_enqueue_styles' );
function ya_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}


//
add_action( 'wp_enqueue_scripts', 'custom_styles', 99 );
function custom_styles() {
    wp_register_style( 'custom-css-style', get_stylesheet_directory_uri() . "/assets/css/custom-css.css",false, 'all' );
    wp_enqueue_style('custom-css-style');
    
    wp_register_script( 'custom-js-script', get_stylesheet_directory_uri() . "/assets/js/custom-js.js", array('jquery'),false,true );
    wp_enqueue_script('custom-js-script');
}


//Tuỳ chỉnh form checkout
function core_customCheckoutFields($fields) {

    $fields['billing']['billing_country']['required'] = false;
    $fields['billing']['billing_city']['required'] = false;
    $fields['billing']['billing_state']['required'] = false;

    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);

    unset($fields['billing']['billing_last_name']);
    // unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['account']['account_password']);

    $fields['billing']['billing_first_name']['label'] = __('Họ và tên');
    $fields['billing']['billing_first_name']['class'] = ['form-row-wide'];
    
    $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    $fields['billing']['billing_address_1']['class'] = ['form-row-wide'];
    $fields['billing']['billing_address_1']['placeholder'] = '';

    $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    $fields['billing']['billing_phone']['class'] = ['form-row-wide'];


    $fields['billing']['billing_email']['label'] = __('Email');
    $fields['billing']['billing_email']['class'] = ['form-row-wide'];

    $fields['order']['order_comments']['label'] = __('Ghi chú đơn hàng');
    $fields['order']['order_comments']['class'] = ['form-row-wide'];
    $fields['order']['order_comments']['placeholder'] = '';
    
    return $fields;
}
add_filter('woocommerce_checkout_fields' , 'core_customCheckoutFields');


// Xoá value mặc định của input form checkout, hoặc muốn điền sẵn thông tin đăng nhập của user vào form
add_filter( 'woocommerce_checkout_get_value', 'core_populating_checkout_fields', 10, 2 );
function core_populating_checkout_fields ( $value, $input ) {
    $token = ( ! empty( $_GET['token'] ) ) ? $_GET['token'] : '';
    // if( 'testtoken' == $token ) {
        // Define your checkout fields  values below in this array (keep the ones you need)
        $checkout_fields = array(
            'billing_first_name'    => ' ',
            // 'billing_last_name'     => 'Wick',
            // 'billing_company'       => 'Murders & co',
            // 'billing_country'       => 'US',
            'billing_address_1'     => ' ',
            // 'billing_address_2'     => 'Royal suite',
            // 'billing_address_3'          => ' ',
            // 'billing_city'          => 'Los Angeles',
            // 'billing_state'         => 'CA',
            // 'billing_postcode'      => '90102',
            'billing_phone'         => ' ',
            'billing_email'         => ' ',
            // 'shipping_first_name'   => 'John',
            // 'shipping_last_name'    => 'Wick',
            // 'shipping_company'      => 'Murders & co',
            // 'shipping_country'      => 'USA',
            // 'shipping_address_1'    => '7 Random street',
            // 'shipping_address_2'    => 'Royal suite',
            // 'shipping_city'         => 'Los Angeles',
            // 'shipping_state'        => 'California',
            // 'shipping_postcode'     => '90102',
            // 'account_password'       => '',
            'order_comments'        => ' ',
        );
        foreach( $checkout_fields as $key_field => $field_value ){
            if( $input == $key_field && ! empty( $field_value ) ){
                // $value = $field_value;
                $value = '';
            }
        }
    // }
    return $value;
}







// Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
// Disables the block editor from managing widgets.
add_filter( 'use_widgets_block_editor', '__return_false' );
// Use Block Editor default for Post
add_filter('use_block_editor_for_post', '__return_false');


// Remove Version Css Js
function core_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
if (!is_admin()) add_filter( 'style_loader_src', 'core_remove_ver_css_js', 9999 );
if (!is_admin()) add_filter( 'script_loader_src', 'core_remove_ver_css_js', 9999 );


// Add Async to the Css Js
function core_add_async_attribute_css($tag, $handle) {
    // $scripts_to_async = array('main-style');
    // foreach($scripts_to_async as $async_script) {
        // if ($async_script === $handle) {
            return str_replace(' href', ' async="async" href', $tag);
        // }
    // }
    return $tag;
}
if (!is_admin()) add_filter('style_loader_tag', 'core_add_async_attribute_css', 10, 2);


// Remove JQuery migrate
// function core_remove_jquery_migrate( $scripts ) {
//     if ( !is_admin() && isset( $scripts->registered['jquery'] ) ) {
//         $script = $scripts->registered['jquery'];
//         if ( $script->deps ) { 
//             $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
//         }
//     }
// }
// add_action( 'wp_default_scripts', 'core_remove_jquery_migrate' );


// Disable Embeds
function core_my_deregister_scripts(){
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'core_my_deregister_scripts' );


// Disable Dashicons
function core_my_deregister_styles()    { 
   wp_deregister_style( 'dashicons' ); 
}
if ( !is_admin() && !is_user_logged_in() ) add_action( 'wp_print_styles', 'core_my_deregister_styles', 100 );
// Disable Admin bar
function core_hide_admin_bar_from_front_end(){
    if (is_blog_admin()) {
        return true;
    }
    return false;
}
if ( !is_admin() && !is_user_logged_in() ) add_filter( 'show_admin_bar', 'core_hide_admin_bar_from_front_end' );


// Disable emojis
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }
    return $urls;
}